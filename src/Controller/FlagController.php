<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function PHPSTORM_META\type;

class FlagController extends AbstractController
{

    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;

    }

    public function getFlag(string $code, CacheInterface $cache):JsonResponse
    {
        $flag = $cache->get($code."-flag", function (ItemInterface $item) use( $code) {
            $item->expiresAfter(3600);
            return $this->fetchFlag($code);
        });

        $response = new JsonResponse();
        $response->setData($flag);

        return $response;
    }

    private function fetchFlag($code){
        try {
            $response = $this->client->request(
                'GET',
                $this->getParameter("restcountries_base_url").$code
            );
            $statusCode = $response->getStatusCode();
            if($statusCode==200){
                $data= json_decode($response->getContent())[0];
                $flag= $data->flags->svg;
                return [ 'success'=>true,'flag'=>$flag];
            }
        } catch (\Exception $e) {
            return [ 'success'=>false,'response'=>$e->getMessage()];
        }
    }
}