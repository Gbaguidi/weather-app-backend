<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WheatherController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;

    }

    public function getWheather(float $longitude,float $latitude, CacheInterface $cache):JsonResponse
    {
        $wheather= $cache->get( strval($longitude)."_".strval($latitude),function (ItemInterface $item) use( $longitude, $latitude){
                $item->expiresAfter(3600);
                return $this->fetchWheather($longitude, $latitude);
            }
        );

        $wheather= $this->fetchWheather( $longitude, $latitude);
        $response = new JsonResponse();
        $response->setData($wheather);
        return $response;
    }


    private function fetchWheather($longitude, $latitude ){
        try {
            $url=$this->getParameter("openweathermap_base_url")."data/2.5/onecall?lon=".$longitude."&lat=".$latitude."&exclude=hourly,minutely&units=metric&appid=".$this->getParameter("openweathermap_api_key");
            $response = $this->client->request(
                'GET', $url
            );
            $statusCode = $response->getStatusCode();
            if($statusCode==200){
                $data= json_decode($response->getContent())->daily;
                $wheathers= $this->cleanWeatherInformation($data);
                return [ 'success'=>true,'wheather'=>$wheathers];
            }
        } catch (\Exception $e) {
            return [ 'success'=>false,'response'=>$e->getMessage()];
        }
    }

    private function cleanWeatherInformation($informations){
        $result=[];
        foreach($informations as $key => $dayWeather){
            $entry=[];
            $formatedDate = date('l d F',$dayWeather->dt);
            $entry=[
                "date" => $formatedDate,
                "temp" => $dayWeather->temp->day,
                "tempDay" => $dayWeather->temp->morn,
                "tempNight" => $dayWeather->temp->night,
                "humidity" => $dayWeather->humidity,
                "pressure" => $dayWeather->pressure,
                "wind" => $dayWeather->wind_speed,
                "icon" => [
                    "description" => $dayWeather->weather[0]->description,
                    "name" => $dayWeather->weather[0]->icon
                ],
            ];
            array_push($result,$entry);
        }
        return $result;
    }
}