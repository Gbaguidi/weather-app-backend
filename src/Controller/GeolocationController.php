<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Cache\ItemInterface;

class GeolocationController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;

    }

    public function getLocation(string $location, CacheInterface $cache):JsonResponse
    {
        $location= $cache->get( $location."-location",function (ItemInterface $item) use( $location){
                $item->expiresAfter(3600);
                return $this->fetchLocation($location);
            }
        );
        $response = new JsonResponse();
        $response->setData($location);
        return $response;
    }

    private function fetchLocation($location){
        try {
            $response = $this->client->request(
                'GET',
                $this->getParameter("openweathermap_base_url")."geo/1.0/direct?q=".$location."&limit=1&appid=".$this->getParameter("openweathermap_api_key")
            );
            $statusCode = $response->getStatusCode();
            if($statusCode==200){
                $location= json_decode($response->getContent());
                if (sizeOf($location)>0) {
                    $location= $this->cleanLocation($location[0]);
                }else{
                    $location=null;
                }
                return [ 'success'=>true,'location'=>$location];
            }
        } catch (\Exception $e) {
            return [ 'success'=>false,'response'=>$e->getMessage()];
        }
    }

    private function cleanLocation($location){
        return [
                "name" => $location->name,
                "lat" => $location->lat,
                "lon" => $location->lon,
                "country" => $location->country,
            ];
    }
}