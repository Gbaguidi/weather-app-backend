# Exercice 2

Temps de réalisation : 2h

## Setup the project
- Cloner le projet avec la commande : 
```
    git clone git@gitlab.com:Gbaguidi/weather-app-backend.git
```
- Déplacer vous dans le répertoire du projet :
```
    cd weather-app-backend
```
- Installer les dépendances avec composer :
```
    composer install
``` 
- Utiliser le fichier **.env.dev** pour créer le fichier **.env** :
```
    cp .env.dev .env
``` 
- Lancer l'application avec la commande :
``` 
symfony server:start
``` 

## Endpoint

### GET /get-flag/{code} :
Retourne une url qui pointe sur une image au format svg qui est le drapeau du pays rechercher  
Paramètres : 
- code : le code ISO3166 à 2 caratères du pays rechercher  
Réponse : 
``` 
{"success":true,"flag":"https:\/\/flagcdn.com\/bj.svg"}
``` 


### GET /get-wheather/{longitude}/{latitude} :
Retourne les prévisions météorologiques de la ville de longintude **longintude** et de latitude **latitude**  
Paramètres : 
- latitude  : la latitude de la localisation rechercher 
- longintude  : la longintude de la localisation rechercher  
Réponse : 
``` 
{"success": true,
    "wheather": [
        {
            "date": "Friday 25 March",
            "temp": 17.26,
            "tempDay": 9.53,
            "tempNight": 12.18,
            "humidity": 30,
            "pressure": 1028,
            "wind": 4.91,
            "icon": {
                "description": "clear sky",
                "name": "01d"
            }
        },
        ...
}
``` 

### GET /geolocation/{location} :
Retourne des informations de localisation de **location**  
Paramètres : 
- location : la loclisation rechercher  
Réponse : 
``` 
{
    "success": true,
    "location": {
        "name": "Cotonou",
        "lat": 6.3676953,
        "lon": 2.4252507,
        "country": "BJ"
    }
}
```  
Lorsqu'il n'y a pas de correspondances le format de réponse est :
``` 
{
    "success": true,
    "location": null
}
``` 

## Principes utilisés

- Un répertoire racine pour tout les controllers
- Un controller par endpoint afin d'isoler les différents traitements
- Un répertoire racine pour toutes les requêtes (vu le nombre de requêtes, nous avons utilisé un seul fichier est suffisant.) 
- Utilisation d'un fichiers yaml pour le routage (de manière générale, il est préférable de centraliser les déclarations des routes autant que possible afin de pouvoir parcourir facilement l'ensemble du projet. De plus la syntaxe YAML permet de rajouter des contrôles très pousser sur les différents paramètres de chaque route)
- L'envoi des réponses au format Json

## Recommandations

- Rajouter des éléments de contrôles sur les différentes routes pour s'assurer que les paramètres sont bien gérés ;
- Mettre en place des services qui permettent de différencier les traitements selon qu'il s'agisse d'un pays ou d'une ville ;
- Implémenter un service d'autocomplétion en se servant en priorité du cache ;
- Gérer l'internationalisation des résultats ;